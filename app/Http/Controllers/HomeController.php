<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Reply;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $comments = Comment::latest('created_at')->get();
        return view('home',compact('comments'));
    }

    public function comment()
    {
        $comments = new Comment;
        $comments->user_id = Auth::user()->id;
        $comments->name = Auth::user()->name;
        $comments->comment = request('comment');

        $comments->save();
        return redirect()->back();

    }

    public function reply()
    {
        $replies = new Reply;
        $replies->comment_id = request('comment_id');
        $replies->user_id = Auth::user()->id;
        $replies->name =  Auth::user()->name;
        $replies->reply = request('reply');

        $replies->save();
        return redirect()->back();
    }
}
