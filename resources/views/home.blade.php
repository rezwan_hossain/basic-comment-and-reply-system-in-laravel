@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    <form method="POST" action="/home/comment">
                        @csrf
                        <textarea class="form-control" name="comment"></textarea>
                        <button class="btn btn-primary btn-lg">submit</button>
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-header">Comment</div>
                <div class="card-body">
                    @foreach($comments as $comment)
                        <div class="someStyle">
                            <i><b>{{ $comment->name }}</b></i>
                            <span>{{ $comment->comment }}</span>
                            <small>{{ $comment->created_at->diffForHumans() }}</small>
                            <div style="margin-left: 15px">
                                <button class="replyButton">reply</button>
                                <div style="display: none;">
                                    <form method="POST" action="/home/reply">
                                        @csrf
                                        <input type="hidden" name="comment_id" value="{{$comment->id}}">
                                        <textarea class="form-control" name="reply"></textarea>
                                        <button class="btn btn-primary" style="background: #3490dc;color: #FFFFFF;padding: 4px 8px;text-decoration: none;">reply</button>
                                    </form>
                                </div>
                                @foreach( $comment->replies as $reply )
                                    @if($comment->id == $reply->comment_id)
                                        <div class="replyStyle">
                                            <i><b>{{ $reply->name }}</b></i>
                                            <span>{{ $reply->reply }}</span>
                                            <small>{{ $reply->created_at->diffForHumans() }}</small>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
